class promedio:

    def devolver_cadena(self,cadena):
        tamano = len(cadena);
        if tamano>0:
            if tamano == 1:
                return 1;
            if len(cadena) > 1:
                if "," in cadena:
                    listaElementos = cadena.split(",");
                    numElementos = len(listaElementos);
                    return numElementos;
            else:
                return cadena;
        else:
            return 0;

    def calcular_minimo(self,cadena):
        listaNumeros = [];

        if len(cadena)>0:
            if len(cadena) > 1:
                if "," in cadena:
                    listaElementos = cadena.split(",");
                    for num in listaElementos:
                        listaNumeros.append(int(num));
                    return min(listaNumeros);
            else:
                return 1;
        else:
            return 0;

    def calcular_maximo(self,cadena):
        listaGeneral = [];
        if len(cadena)>0:
            if len(cadena)>1:
                if "," in cadena:
                    lista = cadena.split(",");
                    for num in lista:
                        listaGeneral.append(int(num));
                    return max(listaGeneral);
            else:
                return int(cadena);
        else:
            return 0;

    def calcular_promedio(self,cadena):
        total = 0;
        if len(cadena) > 0:
            if len(cadena) ==1:
                return int(cadena);
            else:
                if "," in cadena:
                    lista = cadena.split(",");
                    for num in lista:
                        total = total+int(num);
                    resultado = total/float(len(lista));
                    return resultado;
        else:
            return 0;

    def devoler_arreglo(self,cadena):
        arreglo = [];
        arreglo.append(self.devolver_cadena(cadena));
        arreglo.append(self.calcular_minimo(cadena));
        arreglo.append(self.calcular_maximo(cadena));
        arreglo.append(self.calcular_promedio(cadena));
        return arreglo;
