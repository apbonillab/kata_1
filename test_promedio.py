from unittest import TestCase

from promedio import promedio

class TestPromedio(TestCase):

   def test_devolver_cadena(self):
        cadena = "";
        self.assertEqual(promedio().devolver_cadena(cadena),0,"Cadena Vacia");

   def test_devolver_cadena_unelemento(self):
        cadena = "1";
        self.assertEqual(promedio().devolver_cadena(cadena),1,"un elemento");

   def test_devolver_cadena_doselementos(self):
        cadena = "1,2";
        self.assertEqual(promedio().devolver_cadena(cadena),2,"Dos elementos");

   def test_devolver_cadena_deNelementosA(self):
        cadena = "1,2,3,4,5,6,7,8,9,10";
        self.assertEqual(promedio().devolver_cadena(cadena),10,"N elementos A");

   def test_devolver_cadena_deNelementosB(self):
        cadena = "11,12,13,14,15";
        self.assertEqual(promedio().devoler_arreglo(cadena)[0],5,"N elementos B");

   def test_devolver_arreglo_Numeroelementos(self):
        cadena = "11,12,13,14,15";
        self.assertEqual(promedio().devolver_cadena(cadena), 5, "N elementos B");
        self.assertEqual(promedio().devoler_arreglo(cadena)[0],5,"Arreglo con numero elementos");


   def test_devolver_arreglo_qElementosyMinimo(self):
        cadena = "";
        self.assertEqual(promedio().devoler_arreglo(cadena)[1],0,"Arreglo con numero elementos y minimo");


   def test_devolver_arreglo_qElementosyMinimo_unnumero(self):
        cadena = "1";
        self.assertEqual(1,promedio().devoler_arreglo(cadena)[1],"Arreglo con numero elementos y minimo, un numero");



   def test_devolver_arreglo_qElementosyMinimo_2numeros(self):
      cadena = "5,6";
      self.assertEqual(5, promedio().devoler_arreglo(cadena)[1],"Arreglo numero elementos y minimo, dos numeros");


   def test_devolver_arreglo_qElementosyMinimo_Nnumeros(self):
      cadena = "7,6,8,10";
      self.assertEqual(6, promedio().devoler_arreglo(cadena)[1],"Arreglo numero elementos y minimo, N numeros");


   def test_devolver_arreglo_qElementosMinimoMaxima(self):
        cadena = "";
        self.assertEqual(0,promedio().devoler_arreglo(cadena)[2],"Arreglo con numero elementos,el minimo y el maximo");


   def test_devolver_arreglo_qElementosMinMax_unnumero(self):
        cadena = "5";
        self.assertEqual(5,promedio().devoler_arreglo(cadena)[2],"Arreglo con numero elementos, minimo,max, un numero");


   def test_devolver_arreglo_qElementosyMinMax_2numeros(self):
      cadena = "5,6";
      self.assertEqual(6, promedio().devoler_arreglo(cadena)[2],"Arreglo numero elementos, minimo,max, dos numeros");


   def test_devolver_arreglo_qElementosMinMax_Nnumeros(self):
      cadena = "7,6,8,10";
      self.assertEqual(10, promedio().devoler_arreglo(cadena)[2],"Arreglo numero elementos, minimo y max, N numeros");


   def test_devolver_arreglo_qElementosMinMaxProm(self):
        cadena = "";
        self.assertEqual(0,promedio().devoler_arreglo(cadena)[3],"Arreglo con numero elementos, min,max,prom, cadena vacia");


   def test_devolver_arreglo_qElementosMinMaxProm_1num(self):
        cadena = "5";
        self.assertEqual(5,promedio().devoler_arreglo(cadena)[3],"Arreglo con numero elementos, minimo,max,prom, un numero");


   def test_devolver_arreglo_qElementosyMinMaxProm_2numeros(self):
      cadena = "5,6";
      self.assertEqual(5.5, promedio().devoler_arreglo(cadena)[3],"Arreglo numero elementos, minimo,max,prom, dos numeros");


   def test_devolver_arreglo_qElementosMinMax_Nnumeros(self):
      cadena = "7,6,8,10";
      self.assertEqual(7.75, promedio().devoler_arreglo(cadena)[3],"Arreglo numero elementos, minimo y max, N numeros");
